FROM ubuntu

RUN apt-get update \
  && apt-get install -y lftp ca-certificates \
  && apt-get clean \
  && adduser --home /home/lftp --shell /bin/bash --disabled-password -q --gecos "" lftp

USER lftp
